using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections;

public static class DataParser {
    [MenuItem("Assets/Create/Create Actor Data")]
    public static void ParseActor() {
        ScriptableObjectUtility.CreateAsset<ActorData> ();
    }

    [MenuItem("Assets/Create/Create Achievement Data")]
    public static void ParseAchievements() {
        ScriptableObjectUtility.CreateAsset<Achievement> ();
    }
}