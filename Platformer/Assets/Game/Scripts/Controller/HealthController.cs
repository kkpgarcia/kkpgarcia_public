﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour {
	[SerializeField]
	Image HealthBar;

	[SerializeField]
	RectTransform Parent;


	void OnEnable() {
		this.AddObserver(Initialize, Player.OnInitializeNotfication);
		this.AddObserver(OnChangeHealthBar, Player.OnHitNotfication);
	}

	void OnDisable() {
		this.RemoveObserver(Initialize, Player.OnInitializeNotfication);
		this.RemoveObserver(OnChangeHealthBar, Player.OnHitNotfication);
	}

	public void Initialize(object sender, object args) {
		ActorData data = (ActorData)args;

		for(int i = 1; i <= data.HP; i++) {
			GameObject go = Instantiate(HealthBar.gameObject);
			go.transform.SetParent(Parent);
		}
	}

	private void OnChangeHealthBar(object sender, object args) {
		int amount = (int)args;
		for(int i = 1; i <= amount; i++){
			if(Parent.childCount == 0)
				return;
			Destroy(Parent.GetChild(0).gameObject);
		}
	}
}
