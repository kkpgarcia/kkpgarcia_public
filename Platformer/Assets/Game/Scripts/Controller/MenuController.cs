﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {
	public void LoadCharacterSelection() {
		SceneManager.LoadScene("Character Selection");
	}

	public void LoadCredits() {
		SceneManager.LoadScene("Credits");
	}
}
