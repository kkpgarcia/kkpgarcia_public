using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterSelectionController : MonoBehaviour {
    public void MainCharacterLoad() {
        PersistentData.Instance.CharacterLoaded = "MainCharacter";
        LoadGame();
    }

    public void SubCharacterLoad() {
        PersistentData.Instance.CharacterLoaded = "SubCharacter";
        LoadGame();
    }

    private void LoadGame() {
        SceneManager.LoadScene("Game");
    }
}