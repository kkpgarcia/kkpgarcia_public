using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementsController : MonoBehaviour {
    public Dictionary<AchievementTypes, List<Achievement>> Achievements = new Dictionary<AchievementTypes, List<Achievement>>();
    private Dictionary<AchievementTypes, List<string>> m_AchievementsIDs = new Dictionary<AchievementTypes, List<string>>();

    [SerializeField]
    Text Title;
    [SerializeField]
    Text Message;
    [SerializeField]
    Panel Panel;

    void Start() {
        Panel.SetPosition("Hide", false);
    }

    private void OnEnable() {
        this.AddObserver(RecieveNotification, InputController.OnInputEvent);
        this.AddObserver(RecieveNotification, BaseAI.OnEnemyDestroyed);
    }

    private void OnDisable() {
        this.RemoveObserver(RecieveNotification, InputController.OnInputEvent);
        this.RemoveObserver(RecieveNotification, BaseAI.OnEnemyDestroyed);
    }

    public void LoadAchievements() {
        string[] achievements = new string[] {
            "Achievement 1",
            "Achievement 2",
            "Achievement 3"
        };

        foreach(string s in achievements) {
            Achievement a = AchievementFactory.Create(s);
            if(!Achievements.ContainsKey(a.Type))
                Achievements.Add(a.Type, new List<Achievement>());
            if(!m_AchievementsIDs.ContainsKey(a.Type))
                m_AchievementsIDs.Add(a.Type, new List<string>());
            
            Achievements[a.Type].Add(a);
            m_AchievementsIDs[a.Type].Add(a.Id);
        }
    }

    public void RecieveNotification(object sender, object args) {
        AchievementTypes type = (AchievementTypes)args;

        if(!Achievements.ContainsKey(type))
            return;
        if(!m_AchievementsIDs.ContainsKey(type))
            return;

        List<string> temp = new List<string>();
        foreach(string s in m_AchievementsIDs[type]) {
            temp.Add(s);
        }

        foreach(string s in temp) {
            Achievement a = Achievements[type].Find(x => x.Id == s);
            a.Count--;

            if(a.Count > 0)
                return;

            RaiseAchievementUnlocked(a);

            Achievements[type].Remove(a);
            m_AchievementsIDs[type].Remove(s);

            if(Achievements[type].Count == 0) {
                Achievements.Remove(type);
                m_AchievementsIDs.Remove(type);
            }
        }
    }

    private void RaiseAchievementUnlocked(Achievement a) {
        Debug.Log("Achievement Unlocked: " + a.Name + " " + a.Message);
        this.Title.text = a.Name;
        this.Message.text = a.Message;
        
        StartCoroutine(AchievementDisplay());
    }

    IEnumerator AchievementDisplay() {
        Panel.SetPosition("Hide", false);
        Panel.SetPosition("Show", true);
        yield return new WaitForSeconds(4);
        Panel.SetPosition("Hide", true);

        if(Achievements.Count == 0)
            UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
    }
}