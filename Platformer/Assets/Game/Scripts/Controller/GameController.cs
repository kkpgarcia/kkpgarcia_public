﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : StateMachine {
	public Player Player;
	public CameraRig CameraRig;
	public AchievementsController AchievementsController;
	void Start() {
		ChangeState<InitGameState>();
	}
}
