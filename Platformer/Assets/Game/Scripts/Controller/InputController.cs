﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public class InputController : MonoBehaviour {
	public static event EventHandler<InfoEventArgs<int>> HorizontalEvent;
	public static event EventHandler<InfoEventArgs<int>> VerticalEvent;
	public static event EventHandler<InfoEventArgs<int>> AcceptEvent;
	public static event EventHandler<InfoEventArgs<int>> CancelEvent;
	public static event EventHandler<InfoEventArgs<int>> StartEvent;
	//public static event EventHandler<InfoEventArgs<int>> InfoEvent;

	Repeater m_Horizontal = new Repeater ("Horizontal");
	Repeater m_Vertical = new Repeater ("Vertical");
	KeyCode m_Accept = KeyCode.Z;
	KeyCode m_Cancel = KeyCode.X;
	KeyCode m_Start = KeyCode.Return;

	bool m_inputEnabled = false;

	public const string OnInputEvent = "InputController.OnInputEvent";

	public void OnEnable() {
		this.AddObserver(OnStartGame, PlayGameState.StartGame);
	}

	public void OnDisable() {
		this.RemoveObserver(OnStartGame, PlayGameState.StartGame);
	}

	private void OnStartGame(object sender, object args) {
		m_inputEnabled = true;
	}

	void Update() {
		if(!m_inputEnabled)
			return;

		int x = Mathf.RoundToInt(Input.GetAxisRaw("Horizontal"));
		int y = Mathf.RoundToInt(Input.GetAxisRaw("Vertical"));

		if (HorizontalEvent != null){
			HorizontalEvent (this, new InfoEventArgs<int> (x));
			if(x != 0)
				SendNotification();
		}
	
		if (VerticalEvent != null){
			VerticalEvent (this, new InfoEventArgs<int> (x));
			if(y != 0)
				SendNotification();
		}

		if (Input.GetKeyDown (m_Accept)) {
			if (AcceptEvent != null)
				AcceptEvent (this, new InfoEventArgs<int> (1));
			SendNotification();
		}

		if (Input.GetKeyDown (m_Cancel)) {
			if (CancelEvent != null)
				CancelEvent (this, new InfoEventArgs<int> (1));

			SendNotification();
		}

		if (Input.GetKeyDown (m_Start)) {
			if (StartEvent != null)
				StartEvent (this, new InfoEventArgs<int> (1));

			SendNotification();
		}
	}

	private void SendNotification() {
		this.PostNotification(OnInputEvent, AchievementTypes.Input);
	}
}

class Repeater {
	const float TRESHOLD = 0.5f;
	const float RATE = 0.25f;
	float m_Next;
	bool m_Hold;
	string m_Axis;

	public Repeater(string axisName) {
		m_Axis = axisName;
	}

	public int Update() {
		int retValue = 0;
		int value = Mathf.RoundToInt(Input.GetAxisRaw (m_Axis));

		if (value != 0) {
			if (Time.time > m_Next) {
				retValue = value;
				m_Next = Time.time + (m_Hold ? RATE : TRESHOLD);
				m_Hold = true;
			}
		} else {
			m_Hold = false;
			m_Next = 0;
		}

		return retValue;
	}
}