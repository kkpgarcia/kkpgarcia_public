﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayGameState : GameState {

	public const string StartGame = "PlayGameState.StartGame";

	public override void Enter() {
		base.Enter ();
		this.PostNotification(StartGame, null);
	}

	protected override void OnHorizontal(object sender, InfoEventArgs<int> e) {
		Owner.Player.MoveHorizontal(e.Info);
	}

	protected override void OnVertical(object sender, InfoEventArgs<int> e) {

	}

	protected override void OnAccept(object sender, InfoEventArgs<int> e) {
		Owner.Player.OnAction(e.Info);
	}

	protected override void OnCancel(object sender, InfoEventArgs<int> e) {
		Owner.Player.OnJump(e.Info);
	}

	protected override void OnStart(object sender, InfoEventArgs<int> e) {

	}
}
