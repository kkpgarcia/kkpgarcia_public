﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : State {
	protected GameController Owner;

	public Player Player { 
		get { 
			return Owner.Player; 
		} set {
			Owner.Player = value;
		}
	}

	public CameraRig CameraRig {
		get {
			return Owner.CameraRig;
		} set {
			Owner.CameraRig = value;
		}
	}

	protected virtual void Awake() {
		Owner = GetComponent<GameController> ();
	}

	protected override void AddListeners() {
		InputController.HorizontalEvent += OnHorizontal;
		InputController.VerticalEvent += OnVertical;
		InputController.AcceptEvent += OnAccept;
		InputController.CancelEvent += OnCancel;
		InputController.StartEvent += OnStart;
	}

	protected override void RemoveListeners() {
		InputController.HorizontalEvent -= OnHorizontal;
		InputController.VerticalEvent -= OnVertical;
		InputController.AcceptEvent -= OnAccept;
		InputController.CancelEvent -= OnCancel;
		InputController.StartEvent -= OnStart;
	}

	protected virtual void OnHorizontal(object sender, InfoEventArgs<int> e) {

	}

	protected virtual void OnVertical(object sender, InfoEventArgs<int> e) {

	}

	protected virtual void OnAccept(object sender, InfoEventArgs<int> e) {

	}

	protected virtual void OnCancel(object sender, InfoEventArgs<int> e) {

	}

	protected virtual void OnStart(object sender, InfoEventArgs<int> e) {

	}
}
