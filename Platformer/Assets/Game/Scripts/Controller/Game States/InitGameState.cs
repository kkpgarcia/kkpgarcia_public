﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitGameState : GameState {
	public override void Enter() {
		base.Enter ();
		StartCoroutine (Init ());
	}

	IEnumerator Init() {
		yield return StartCoroutine(SpawnActors());
		Owner.AchievementsController.LoadAchievements();
		yield return new WaitForSeconds(1);
		Owner.ChangeState<PlayGameState> ();
	}

	IEnumerator SpawnActors() {
		string[] actors = new string[] {
			PersistentData.Instance.CharacterLoaded,
			"Enemy 1",
			"Enemy 2",
			"Enemy 3",
			"Enemy 4",
			"Enemy 5",
			"Enemy 6",
		};

		GameObject actorContainer = new GameObject("Actors");
		actorContainer.transform.SetParent(Owner.transform);

		for(int i = 0; i < actors.Length; ++i) {
			GameObject instance = ActorFactory.Create(actors[i]);
			if(i == 0){
				Owner.CameraRig.Follow = instance.transform;
				Owner.Player = instance.GetComponent<Player>();
			}
			yield return null;
		}
	}
}
