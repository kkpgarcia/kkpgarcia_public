﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
Simple and quick singleton implementation that persists data 
on different scenes.
 */
public class PersistentData : MonoBehaviour {
	[HideInInspector]
	public string CharacterLoaded;

	private static PersistentData m_Instance = null;
	public static PersistentData Instance {
		get {
			if(m_Instance == null)
				m_Instance = (PersistentData)FindObjectOfType(typeof(PersistentData));
			
			return m_Instance;
		}
	}

	public void Awake() {
		DontDestroyOnLoad(this);
	}
}
