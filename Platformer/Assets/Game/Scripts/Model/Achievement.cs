using System;
using UnityEngine;

/*
Achievement model for customized assets
 */
public class Achievement : ScriptableObject {
    public string Id;
    public string Name;
    public AchievementTypes Type;
    public int Count;
    public string Message;
}