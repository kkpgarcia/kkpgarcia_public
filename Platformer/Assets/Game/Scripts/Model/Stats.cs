using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
Simple stats(attribute) container for actors
 */
public class Stats : MonoBehaviour {
    /*
    Returns value specified by types
     */
    public int this[StatTypes s] {
        get {
            return m_Data[(int)s];
        }
    }

    /*
    Value container for status
     */
    int[] m_Data = new int[(int)StatTypes.Count];

    public void SetValue(StatTypes type, int value) {
        int oldValue = this[type];
        if(oldValue == value)
            return;

        m_Data[(int)type] = value;
    }
}