using UnityEngine;
/*
Actor Data model for customized assets
 */
public class ActorData : ScriptableObject {
    public int HP;
    public int MHP;
    public int Power;
    public int AttackSpeed;
    public int Speed;
    public int JumpForce;
    public string Model;
    public Vector3 InitialPosition;
}