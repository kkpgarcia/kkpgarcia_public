using UnityEngine;
using System.IO;
using System.Collections;

public static class ActorFactory {

    /*
    Loads the asset data from the resources
     */
    public static GameObject Create(string name) {
        ActorData data = Resources.Load<ActorData>("Actor Data/" + name);
        if(data == null) {
            Debug.LogError("No Data for name: " + name);
            return null;
        }

        return Create(data);
    }

    /*
    Gets the prefab model from the resources
     */
    public static GameObject Create(ActorData data) {
        GameObject obj = InstantiatePrefab("Actors/" + data.Model, data);
        return obj;
    }

    /*
    Instantiates and initializes the actor
     */
    static GameObject InstantiatePrefab(string name, ActorData data) {
        GameObject prefab = Resources.Load<GameObject>(name);
        if(prefab == null) {
            Debug.LogError("No Prefab for name: " + name);
            return new GameObject(name);
        }


        GameObject instance = GameObject.Instantiate(prefab, data.InitialPosition, new Quaternion());
        BaseActor actor = instance.GetComponent<BaseActor>();
        if(actor != null)
            actor.ApplyStats(data);
        instance.name = instance.name.Replace("(Clone)", "");
        
        return instance;
    }
}