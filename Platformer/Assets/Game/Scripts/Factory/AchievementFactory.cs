using UnityEngine;
using System.IO;
using System.Collections;

public static class AchievementFactory {

    /*
    Loads the achievement data from the resources
     */
    public static Achievement Create(string name) {
        Achievement data = Resources.Load<Achievement>("Achievement Data/" + name);
        if(data == null) {
            Debug.LogError("No Data for name: " + name);
            return null;
        }

        return data;
    }
}