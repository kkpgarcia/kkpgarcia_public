using System;
using System.Collections.Generic;

using UnityEngine;

public class FSM {
    /*
    Owner of the FSM
     */
    private readonly string m_Name;
    public string Name {
        get {
            return m_Name;
        }
    }
    
    /*
    Current State of the actor who uses the FSM
     */
    private IFSMState m_CurrentState;

    /*
    All the states that the Actor can transition to are saved here.
     */
    private readonly Dictionary<string, IFSMState> m_StateMap;

    private Dictionary<string, IFSMState> m_GlobalTransitionMap;

    /*
    Constructs the FSM and will be given a unique ID;
     */
    public FSM(string name) {
        this.m_Name = name;
        m_CurrentState = null;
        m_StateMap = new Dictionary<string, IFSMState>();
    }

    /*
    Delegate that executes the action.
     */
    private delegate void StateActionProcessor(IFSMAction action);

    /*
    Function that iterates every actions under a state, and executes it using the StateActionProcessor
     */
    private void ProcessStateActions(IFSMState state, StateActionProcessor actionProcessor) {
        IFSMState currentStateOnInvoke = this.m_CurrentState;
        IEnumerable<IFSMAction> actions = state.GetActions();
        foreach(IFSMAction action in actions) {
            actionProcessor(action);
            
            if(this.m_CurrentState != currentStateOnInvoke)
                break;
        }
    }

    /*
    Initializes the state. Checks if it exists and transitions to the state provided by the actor.
     */
    public void Start(string stateName) {
        if(!m_StateMap.ContainsKey(stateName))
            Debug.LogWarning("FSM doesn't contain a state named " + stateName);
        
        ChangeToState(m_StateMap[stateName]);
    }

    /*
    Adds new state
     */
    public IFSMState AddState(string name) {
        if(m_StateMap.ContainsKey(name))
            Debug.LogWarning("The FSM already contains: " + name);

        IFSMState newState = new BaseFSMState(name, this);
        m_StateMap[name] = newState;
        return newState;
    }

    /*
    Transitions to a new state
     */
    private void ChangeToState(IFSMState state) {
        if(this.m_CurrentState != null)
            ExitState(this.m_CurrentState);

        this.m_CurrentState = state;
        EnterState(this.m_CurrentState);
    }

    /*
    Calls all actions inside the state to initiate
     */
    private void EnterState(IFSMState state) {
        ProcessStateActions(state, delegate(IFSMAction action){
            action.OnEnter();
        });
    }

    /*
    Calls all actions inside the state to exit
     */
    private void ExitState(IFSMState state) {
        IFSMState currentStateOnInvoke = this.m_CurrentState;

        ProcessStateActions(state, delegate(IFSMAction action) {
            action.OnExit();
            if(this.m_CurrentState != currentStateOnInvoke) {
                throw new Exception("State cannot be changed on exit of the specified state.");
            }
        });
    }

    /*
    Calls all actions inside the state to update
     */
    public void Update() {
        if(this.m_CurrentState == null) {
            return;
        }

        ProcessStateActions(this.m_CurrentState, delegate(IFSMAction action) {
            action.OnUpdate();
        });
    }

    /*
    Calls all actions inside the state to update on a fixed manner for physics
     */
    public void FixedUpdate() {
        if(this.m_CurrentState == null) {
            return;
        }

        ProcessStateActions(this.m_CurrentState, delegate (IFSMAction action) {
            action.OnFixedUpdate();
        });
    }

    /*
    Returns the current state
     */

    public IFSMState GetCurrentState() {
        return this.m_CurrentState;
    }

    /*
    Tells the FSM to transition to a specific state
     */
    public void SendEvent(string eventId) {
        if(string.IsNullOrEmpty(eventId)) 
            Debug.LogWarning("The specified event ID cannot be empty");

        if(m_CurrentState == null) {
            Debug.LogWarning(string.Format("FSM {0} doesn't have a current state.", this.Name));
            return;
        }
        IFSMState transitionState = ResolveTransition (eventId);

        if (transitionState == null) {
            Debug.LogWarning (string.Format ("The current state {0} has no transition for event {1}", this.m_CurrentState.GetName (), eventId));
        } else {
            ChangeToState (transitionState);
        }
    }

    /*
    Resolves transition
     */
    private IFSMState ResolveTransition(string eventId) {
        IFSMState transitionState = this.m_CurrentState.GetTransition(eventId);

        if(transitionState == null) {
            if(this.m_GlobalTransitionMap != null && this.m_GlobalTransitionMap.ContainsKey(eventId))
                return this.m_GlobalTransitionMap[eventId];
        } else
            return transitionState;
        
        return null;
    }

    /*
    Adds new global transition
     */
    public void AddGlobalTransition(string eventId, IFSMState destinationState) {
        if(this.m_GlobalTransitionMap == null)
            this.m_GlobalTransitionMap = new Dictionary<string, IFSMState>();
        if(!this.m_GlobalTransitionMap.ContainsKey(eventId))
            this.m_GlobalTransitionMap[eventId] = destinationState;
    }
}