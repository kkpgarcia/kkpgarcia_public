using System;
using System.Collections;
using System.Collections.Generic;

/*
Interfaces for the State
 */
public interface IFSMState {
    string GetName();
    void AddTransition(string eventId, IFSMState destinationState);
    IFSMState GetTransition(string eventId);
    void AddAction(IFSMAction action);
    IEnumerable<IFSMAction> GetActions();
    void SendEvent(string eventId);
}