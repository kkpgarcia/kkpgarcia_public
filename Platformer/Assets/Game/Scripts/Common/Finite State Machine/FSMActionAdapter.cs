using System;
using System.Collections;
using System.Collections.Generic;

/*
Adapts the State to Action
 */
public abstract class FSMActionAdapter : IFSMAction {
    private readonly IFSMState m_Owner;

    public FSMActionAdapter(IFSMState owner){
        this.m_Owner = owner;
    }

    public IFSMState GetOwner() {
        return m_Owner;
    }

    public virtual void OnEnter() {}
    public virtual void OnUpdate() {}
    public virtual void OnFixedUpdate() {}
    public virtual void OnExit() {}
}