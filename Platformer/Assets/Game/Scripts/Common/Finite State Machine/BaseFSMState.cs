using System;
using System.Collections;
using System.Collections.Generic;

/*
Base Class implementations for the State. Assigns the most common variables that are essential for the state.
 */
public class BaseFSMState : IFSMState {
    /*
    Name of the State
     */
    private readonly string m_Name;
    
    /*
    Owner of the State
     */
    private readonly FSM m_Owner;

    /*
    Transition map for this state
     */

    private readonly Dictionary<string, IFSMState> m_transitionMap;
    
    /*
    List of actions that are included in this state
     */
    private readonly List<IFSMAction> actionList;

    public BaseFSMState(string name, FSM owner) {
        this.m_Name = name;
        this.m_Owner = owner;
        this.m_transitionMap = new Dictionary<string, IFSMState>();    
        this.actionList = new List<IFSMAction>();
    }

    public string GetName() {
        return m_Name;
    }

    public void AddTransition(string eventId, IFSMState destinationState) {
        if(m_transitionMap.ContainsKey(eventId)) {
            UnityEngine.Debug.LogError(string.Format("The state {0} already contains a transition for event", eventId));
            return;
        }

        m_transitionMap[eventId] = destinationState;
    }

    public IFSMState GetTransition(string eventId) {
        if(m_transitionMap.ContainsKey(eventId))
            return m_transitionMap[eventId];

        return null;
    }

    public void AddAction(IFSMAction action) {
        if(actionList.Contains(action)) {
            UnityEngine.Debug.LogError("The state already contains the specified action");
            return;
        }

        if(action.GetOwner() != this) {
            UnityEngine.Debug.LogError("The owner of the action should be this state.");
            return;
        }

        actionList.Add(action);
    }

    public IEnumerable<IFSMAction> GetActions() {
        return actionList;
    }

    public void SendEvent(string eventId) {
        this.m_Owner.SendEvent(eventId);
    }
}