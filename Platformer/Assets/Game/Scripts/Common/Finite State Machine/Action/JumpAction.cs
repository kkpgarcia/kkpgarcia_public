using UnityEngine;
using System.Collections;

public class JumpAction : BaseRigidbodyAction {
    protected Vector3 m_JumpForce;
    private GroundChecker m_GroundChecker;
    
    public JumpAction(IFSMState owner) : base(owner) { }

    public JumpAction(IFSMState owner, string timeReferenceName) : base(owner) {
        this.m_Timer = new CountdownTimer(1, timeReferenceName);
    }

    public void Init(Rigidbody rigidbody, Vector3 jumpForce, GroundChecker groundChecker, float duration, string finishEvent, Space space = Space.World) {
        this.m_Rigidbody = rigidbody;
        this.m_JumpForce = jumpForce;
        this.m_Duration = duration;
        this.m_GroundChecker = groundChecker;
        this.m_FinishEvent = finishEvent;
        this.m_Space = space;
    }

    public override void OnEnter() {
        if(Comparison.TolerantEquals(m_Duration, 0)) {
            Finish();
            return;
        }
        Jump(this.m_JumpForce);

        m_Timer.Reset(this.m_Duration);
    }

    public override void OnUpdate() {
        m_Timer.Update();

        if(m_Timer.HasElapsed() && m_GroundChecker.IsGrounded) {
            Finish();
            return;
        }
    }

    public override void OnFixedUpdate() {
        if(m_Timer.HasElapsed())
            return;
        if(m_GroundChecker.IsGrounded)
            Jump(m_JumpForce);
    }

    protected void Finish() {
        Jump(this.m_JumpForce);

        if(!string.IsNullOrEmpty(m_FinishEvent)) {
            GetOwner().SendEvent(m_FinishEvent);
        }
    }

    protected void Jump(Vector3 force) {
        switch(this.m_Space) {
            case Space.World:
                m_Rigidbody.AddForce(m_JumpForce, ForceMode.Impulse);
                break;
            case Space.Self:
                m_Rigidbody.AddForce(m_JumpForce, ForceMode.Impulse);
                break;
        }
    }
}