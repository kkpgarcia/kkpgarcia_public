public class BaseFSMAction : FSMActionAdapter {
    protected float m_Duration;
    protected string m_FinishEvent;
    protected string m_TimeReference;
    protected CountdownTimer m_Timer;

    public BaseFSMAction(IFSMState owner) : base(owner) {
        this.m_Timer = new CountdownTimer(1);
    }
}