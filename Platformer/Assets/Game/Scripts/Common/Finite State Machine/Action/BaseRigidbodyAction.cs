using UnityEngine;

public class BaseRigidbodyAction : BaseFSMAction {
    protected Rigidbody m_Rigidbody;
    protected Space m_Space;
    public BaseRigidbodyAction(IFSMState owner) : base(owner) {}
}