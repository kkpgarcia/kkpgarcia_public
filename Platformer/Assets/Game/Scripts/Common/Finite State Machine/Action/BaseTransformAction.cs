using UnityEngine;

public class BaseTransformAction : BaseFSMAction {
    protected Transform m_Transform;
    protected Space m_Space;
    public BaseTransformAction(IFSMState owner) : base(owner) {}
}