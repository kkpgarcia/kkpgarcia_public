using UnityEngine;
using System.Collections;

public class MoveFromPositionAction : MoveAction {
    public MoveFromPositionAction(IFSMState owner) : base(owner) {}

    public MoveFromPositionAction(IFSMState owner, string timeReferenceName) : base(owner, timeReferenceName) {}

    public void Init(Transform transform, Vector3 positionTo, float duration, string finishEvent, Space space = Space.World) {
        base.Init(transform, transform.position, positionTo, duration, finishEvent, space);
    }

    public override void OnEnter() {
        m_PositionFrom = this.m_Transform.position;
        base.OnEnter();
    }

    public override void OnUpdate() {
        base.OnUpdate();
    }
}