using UnityEngine;
using System.Collections;

public class MoveAction : BaseTransformAction {
    protected Vector3 m_PositionFrom;
    protected Vector3 m_PositionTo;
    public MoveAction(IFSMState owner) : base(owner) { }

    public MoveAction(IFSMState owner, string timeReferenceName) : base(owner) {
        this.m_Timer = new CountdownTimer(1, timeReferenceName);
    }

    public void Init(Transform transform, Vector3 positionFrom, Vector3 positionTo, float duration, string finishEvent, Space space = Space.World) {
        this.m_Transform = transform;
        this.m_PositionFrom = positionFrom;
        this.m_PositionTo = positionTo;
        this.m_Duration = duration;
        this.m_FinishEvent = finishEvent;
        this.m_Space = space;
    }

    public override void OnEnter() {
        if(Comparison.TolerantEquals(m_Duration, 0)) {
            Finish();
            return;
        }
        SetPosition(this.m_PositionFrom);

        m_Timer.Reset(this.m_Duration);
    }

    public override void OnUpdate() {
        m_Timer.Update();

        if(m_Timer.HasElapsed()) {
            Finish();
            return;
        }

        SetPosition(Vector3.Lerp(this.m_PositionFrom, this.m_PositionTo, m_Timer.GetRatio()));
    }

    protected void Finish() {
        SetPosition(this.m_PositionTo);

        if(!string.IsNullOrEmpty(m_FinishEvent)) {
            GetOwner().SendEvent(m_FinishEvent);
        }
    }

    protected void SetPosition(Vector3 position) {
        switch(this.m_Space) {
            case Space.World:
                this.m_Transform.position = position;
                break;
            case Space.Self:
                this.m_Transform.position = position;
                break;
        }
    }
}