using UnityEngine;
using System.Collections;

public class FSMShootAction : BaseFSMAction {
    private Bullet m_Bullet;
    private Vector3 m_StartPosition;
    private Quaternion m_StartRotation;
    private float m_FireRate;
    private float m_Next;
    public FSMShootAction(IFSMState owner) : base(owner) { }

    public void Init(Bullet bullet, Vector3 position, Quaternion rotation, float fireRate, float duration, string finishEvent) {
        this.m_Bullet = bullet;
        this.m_StartPosition = position;
        this.m_StartRotation = rotation;
        this.m_FinishEvent = finishEvent;
        this.m_Duration = duration;
        this.m_FireRate = fireRate;
    }

    public override void OnEnter() {
        if(Comparison.TolerantEquals(m_Duration, 0)) {
            Finish();
            return;
        }

        m_Timer.Reset(this.m_Duration);
    }

    public override void OnUpdate(){
        m_Timer.Update();

        if(m_Timer.HasElapsed()) {
            Finish();
            return;
        }

        if(Time.time > m_Next) {
            Bullet b = GameObject.Instantiate(m_Bullet.gameObject, m_StartPosition, m_StartRotation).GetComponent<Bullet>();
            b.Damage = m_Bullet.Damage;
            b.ProjectileSpeed = m_Bullet.ProjectileSpeed;
            m_Next = Time.time + m_FireRate;
        }
    }

    protected void Finish() {
         if(!string.IsNullOrEmpty(m_FinishEvent)) {
            GetOwner().SendEvent(m_FinishEvent);
        }
    }
}