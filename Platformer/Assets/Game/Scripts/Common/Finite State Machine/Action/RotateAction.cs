using System;
using UnityEngine;

public class RotateAction : BaseTransformAction {

    private Quaternion m_quatFrom;
    private Quaternion m_quatTo;
    
    public RotateAction(IFSMState owner) : base(owner){}

    public RotateAction(IFSMState owner, string timerReferenceName) : base(owner) {
        this.m_Timer = new CountdownTimer(1, timerReferenceName);
    }

    public void Init(Transform transform, Quaternion quatFrom, Quaternion quatTo, float duration, string finishEvent) {
        this.m_Transform = transform;
        this.m_quatFrom = quatFrom;
        this.m_quatTo = quatTo;
        this.m_Duration = duration;
        this.m_FinishEvent = finishEvent;
    }

    public override void OnEnter() {
        if(Comparison.TolerantEquals(this.m_Duration, 0)) {
            Finish();
            return;
        }

        if(Quaternion.Equals(this.m_quatFrom, this.m_quatTo)) {
            Finish();
            return;
        }

        this.m_Transform.rotation = this.m_quatFrom;
        m_Timer.Reset(this.m_Duration);
    }

    public override void OnUpdate() {
        this.m_Timer.Update();
        
        if(this.m_Timer.HasElapsed()) {
            Finish();
            return;
        }
    }

    public void Finish() {
        this.m_Transform.rotation = this.m_quatTo;

        if(!string.IsNullOrEmpty(m_FinishEvent)) {
            GetOwner().SendEvent(m_FinishEvent);
        }
    }
}