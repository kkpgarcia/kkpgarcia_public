using System;
using System.Collections;
using System.Collections.Generic;

/*
Interface for all actions that can be used inside the FSM
 */
public interface IFSMAction {
    IFSMState GetOwner();
    void OnEnter();
    void OnUpdate();
    void OnFixedUpdate();
    void OnExit();
}