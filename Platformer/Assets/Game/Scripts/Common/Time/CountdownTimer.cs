using UnityEngine;

public class CountdownTimer {
    private float m_polledTime;
    private float m_countdownTime;

    private TimeReference m_timeReference;

    public CountdownTimer(float countdownTime, TimeReference timeReference) {
        this.m_timeReference = timeReference;

        if(countdownTime < 0)
            Debug.LogWarning("The specified time must be greater than zero.");

        Reset(countdownTime);
    }

    public CountdownTimer(float countdownTime, string timeReferenceName) : this(countdownTime, TimeReferencePool.GetInstance().Get(timeReferenceName)) {}

    public CountdownTimer(float countdownTime) : this(countdownTime, TimeReference.GetDefaultInstance()) {}

    public void Update() {
        this.m_polledTime += this.m_timeReference.DeltaTime;
    }

    public void Reset() {
        this.m_polledTime = 0;
    }

    public void Reset(float countdownTime) {
        Reset();
        this.m_countdownTime = countdownTime;
    }

    public bool HasElapsed() {
        return Comparison.TolerantGreaterThanOrEquals(this.m_polledTime, this.m_countdownTime);
    }

    public float GetRatio() {
        float ratio = this.m_polledTime / this.m_countdownTime;
        return Mathf.Clamp(ratio, 0, 1);
    }

    public float GetPolledTime() {
        return this.m_polledTime;
    }

    public void SetPolledTime(float polledTime) {
        this.m_polledTime = polledTime;
    }

    public void EndTimer() {
        this.m_polledTime = this.m_countdownTime;
    }

    public void SetCountdownTime(float newTime) {
        this.m_countdownTime = newTime;
    }

    public float GetCountdownTime() {
        return this.m_countdownTime;
    }

    public string GetCountdownTimeString() {
        float timeRemaining = m_countdownTime - m_polledTime;
        int minutes = (int)(timeRemaining / 60.0f);
        int seconds = ((int)timeRemaining) % 60;
        return string.Format ("{0}:{1:d2}", minutes, seconds);
    }
}