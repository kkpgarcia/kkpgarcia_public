using System;
using UnityEngine;

public class TimeReference {
    private string m_Name;

    private float m_TimeScale;

    private bool m_AffectsGlobalTimeScale;


    public TimeReference(string name) {
        this.m_Name = name;
        this.m_TimeScale = 1;
        this.m_AffectsGlobalTimeScale = false;
    }

    public string Name {
        get {
            return m_Name;
        }
    }

    public float TimeScale {
       get {
            if(Comparison.TolerantEquals (Time.timeScale, 0))
                return this.m_TimeScale;

            return this.m_TimeScale / Time.timeScale;
       } set {
            if(this.m_AffectsGlobalTimeScale)
                Time.timeScale = value;
            
            this.m_TimeScale = value;
       }
    }

    public float DeltaTime {
        get {
            if(Comparison.TolerantEquals(Time.timeScale, 0))
                return Time.fixedDeltaTime * this.TimeScale;
            return Time.deltaTime * TimeScale;
        }
    }

    private static TimeReference m_DefaultInstance;

    public static TimeReference GetDefaultInstance() {
        if(m_DefaultInstance == null)
            m_DefaultInstance = new TimeReference("Default");

        return m_DefaultInstance;
    }

    public bool AffectsGlobalTimeScale {
        get {
            return m_AffectsGlobalTimeScale;
        } set {
            m_AffectsGlobalTimeScale = true;
        }
    }
}