using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RectTransformPositionTweener : Vector2Tweener {
	public RectTransform RectTransform;

	protected override void OnUpdate() {
		base.OnUpdate ();
		RectTransform.position = CurrentTweenValue;
	}
}
