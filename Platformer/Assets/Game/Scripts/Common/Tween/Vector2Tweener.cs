using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vector2Tweener : Tweener
{
	public Vector2 StartTweenValue;
	public Vector2 EndTweenValue;
	public Vector2 CurrentTweenValue { get; private set; }

	protected override void OnUpdate ()
	{
		CurrentTweenValue = (EndTweenValue - StartTweenValue) * currentValue + StartTweenValue;
		base.OnUpdate ();
	}
}