﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMovement : MonoBehaviour {
	[SerializeField]
	Vector3 MinPosition;

	[SerializeField]
	Vector3 MaxPosition;

	FSM m_FSM;
	IFSMState MoveMinState;
	IFSMState MoveMaxState;
	MoveFromPositionAction MoveMin;
	MoveFromPositionAction MoveMax;
	private void Start() {
		m_FSM = new FSM("Platform");
		MoveMinState = m_FSM.AddState("MoveMinState");
		MoveMaxState = m_FSM.AddState("MoveMaxState");

		MoveMin = new MoveFromPositionAction(MoveMinState);
		MoveMax = new MoveFromPositionAction(MoveMaxState);

		MoveMinState.AddTransition("MoveMaxState", MoveMaxState);
		MoveMaxState.AddTransition("MoveMinState", MoveMinState);

		MoveMinState.AddAction(MoveMin);
		MoveMaxState.AddAction(MoveMax);

		MoveMin.Init(this.transform, MinPosition, 2, "MoveMaxState");
		MoveMax.Init(this.transform, MaxPosition, 2, "MoveMinState");

		m_FSM.Start("MoveMaxState");
	}

	public void Update() {
		m_FSM.Update();
	}
}
