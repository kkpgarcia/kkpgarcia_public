using UnityEngine;
using System;
using System.Collections;

/*
Simple look at class
 */
public class CameraRig : MonoBehaviour {
    
    /*
    Speed of the object when following another object
     */
    public float Speed;

    /*
    The target object to be followed
     */
    public Transform Follow;

    void Update ()
	{
        if(Follow != null)
		    this.transform.position = Vector3.Lerp(this.transform.position, Follow.position, Speed * Time.deltaTime);
	}
}