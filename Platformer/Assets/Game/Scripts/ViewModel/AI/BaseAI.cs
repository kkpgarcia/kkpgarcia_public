﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
Base class for all AI Actors
 */
public class BaseAI : BaseActor {
	/*
	Reference to player's transform
	 */
	protected Transform Player;

	/*
	Reference to this actor's view
	 */
	protected GameObject View;

	/*
	Finite State Machine of this AI
	 */
	protected FSM m_FSM;
	protected IFSMState MoveLeftState;
	protected IFSMState MoveRightState;

	/*
	Post Notification message when destroyed
	 */
	public const string OnEnemyDestroyed = "BaseAI.OnEnemyDestroyed";

	/*
	Initializes the AI
	 */
	protected virtual void Start() {
		m_FSM = new FSM("Base AI");
		Player = GameObject.FindGameObjectWithTag("Player").transform;
		View = this.transform.Find("View").gameObject;
	}

	/*
	Updates the Finite State Machine
	*/	
	protected virtual void Update() {
		m_FSM.Update();
	}

	/*
	Checks if the Player if on the left side, or right side of AI
	 */
	protected float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up) {
		Vector3 perp = Vector3.Cross(fwd, targetDir);
		float dir = Vector3.Dot(perp, up);
		
		if (dir > 0f) {
			return 1f;
		} else if (dir < 0f) {
			return -1f;
		} else {
			return 0f;
		}
	}

	public override void ApplyStats(ActorData data) { 
		base.ApplyStats(data);
	}

	private void OnTriggerEnter(Collider collider) {
		GameObject go = collider.gameObject;
		if(go.tag == "Bullet"){
			Bullet bullet = go.GetComponent<Bullet>();
			OnHit(bullet.Damage);
			Destroy(go);
		}
	}

	protected void OnHit(int damage) {
		this.Stats[(int)StatTypes.HP] -= damage;
		if(this.Stats[(int)StatTypes.HP] <= 0) {
			DestroyAI();
			return;
		}
	}

	protected void DestroyAI() {
		this.PostNotification(OnEnemyDestroyed, AchievementTypes.Score);
		Destroy(this.gameObject);
	}
}
