using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMovingAI : BaseAI {
    protected MoveFromPositionAction MoveLeftAction;
	protected MoveFromPositionAction MoveRightAction;
	protected RotateAction RotateLeftAction;
	protected RotateAction RotateRightAction;

	/*
	Initializes all the actions
	 */
    protected override void Start() {
        base.Start();
        MoveLeftState = m_FSM.AddState("MoveLeftState");
		MoveRightState = m_FSM.AddState("MoveRightState");

        MoveLeftAction = new MoveFromPositionAction(MoveLeftState);
		RotateLeftAction = new RotateAction(MoveLeftState);
		MoveRightAction = new MoveFromPositionAction(MoveRightState);
		RotateRightAction = new RotateAction(MoveRightState);

        MoveRightState.AddAction(MoveRightAction);
		MoveRightState.AddAction(RotateRightAction);
		MoveLeftState.AddAction(MoveLeftAction);
		MoveLeftState.AddAction(RotateLeftAction);

        MoveLeftState.AddTransition ("ToRight", MoveRightState);
		MoveRightState.AddTransition ("ToLeft", MoveLeftState);

        MoveRightAction.Init(this.transform, new Vector3(this.transform.position.x - 5, this.transform.position.y,this.transform.position.z), 3.0f, "ToLeft");
		MoveLeftAction.Init(this.transform, new Vector3(this.transform.position.x + 5, this.transform.position.y,this.transform.position.z), 3.0f, "ToRight");
		RotateRightAction.Init(View.transform, this.transform.rotation, new Quaternion(0, 180, 0, 0), 0f, "");
		RotateLeftAction.Init(View.transform, new Quaternion(0, 180, 0, 0), new Quaternion(0, 0, 0, 0), 0f, "");
    }
}
