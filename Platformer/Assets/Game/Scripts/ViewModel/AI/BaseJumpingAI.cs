using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseJumpingAI : BaseAI {
    protected JumpAction MoveLeftAction;
	protected JumpAction MoveRightAction;
	protected RotateAction RotateLeftAction;
	protected RotateAction RotateRightAction;

    public GroundChecker GroundChecker;
    public Rigidbody Rigidbody;

    
	/*
	Initializes all the actions
	 */
    protected override void Start() {
        base.Start();
        MoveLeftState = m_FSM.AddState("MoveLeftState");
		MoveRightState = m_FSM.AddState("MoveRightState");

        MoveLeftAction = new JumpAction(MoveLeftState);
		RotateLeftAction = new RotateAction(MoveLeftState);
		MoveRightAction = new JumpAction(MoveRightState);
		RotateRightAction = new RotateAction(MoveRightState);

        MoveRightState.AddAction(MoveRightAction);
		MoveRightState.AddAction(RotateRightAction);
		MoveLeftState.AddAction(MoveLeftAction);
		MoveLeftState.AddAction(RotateLeftAction);

        MoveLeftState.AddTransition ("ToRight", MoveRightState);
		MoveRightState.AddTransition ("ToLeft", MoveLeftState);

        MoveRightAction.Init(this.Rigidbody, new Vector3(1,3,0), GroundChecker, 1.0f, "ToLeft");
		MoveLeftAction.Init(this.Rigidbody, new Vector3(-1,3,0), GroundChecker, 1.0f, "ToRight");
		RotateRightAction.Init(View.transform, this.transform.rotation, new Quaternion(0, 180, 0, 0), 0f, "");
		RotateLeftAction.Init(View.transform, new Quaternion(0, 180, 0, 0), new Quaternion(0, 0, 0, 0), 0f, "");

        m_FSM.Start("MoveLeftState");
    }

    /*
    Updates the Action that needs Physics time
     */
    protected virtual void FixedUpdate() {
		m_FSM.FixedUpdate();
	}

}
