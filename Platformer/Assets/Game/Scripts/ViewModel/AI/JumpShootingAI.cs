using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpShootingAI : BaseJumpingAI {
    public HomingBullet Bullet;
    public Transform GunPosition;

    private IFSMState ShootState;
    private FSMShootAction ShootAction;

    /*
	Initializes all the actions
	 */
     protected override void Start() {
        base.Start();
		
		ShootState = m_FSM.AddState("ShootState");
		ShootAction = new FSMShootAction(ShootState);
		ShootState.AddAction(ShootAction);
		
		MoveLeftState.AddTransition("Shoot", ShootState);
		MoveRightState.AddTransition("Shoot", ShootState);
		
		ShootState.AddTransition ("ToRight", MoveRightState);
		ShootState.AddTransition ("ToLeft", MoveLeftState);
		
		m_FSM.Start("MoveLeftState");
	}

    protected override void Update() {
		if(Player == null)
			return;
        /*
        Checks the distance from the AI to the Player
         */
		float distanceToPlayer = Vector3.Distance(Player.position, this.transform.position);
		if(distanceToPlayer < 5) {
			Vector3 heading = Player.position - transform.position;
			float angleDir = AngleDir(transform.forward, heading, transform.up);
			string directionEvent = "";
			
            /*
            Check the direction then look left. Else, look right
             */
			if (angleDir > 0){
				View.transform.rotation = new Quaternion(0, 0, 0, 0);
				directionEvent = "ToLeft";
			} else if(angleDir < 0){
				View.transform.rotation = new Quaternion(0, 180, 0, 0);
				directionEvent = "ToRight";
			}

            /*
            Check if the state is not shoot, then changes to shoot state
             */
			if(m_FSM.GetCurrentState().GetName() != "ShootState") {
				this.Bullet.Damage = Stats[(int)StatTypes.POWER];
				this.Bullet.ProjectileSpeed = Stats[(int)StatTypes.AttackSpeed];
				ShootAction.Init(this.Bullet, this.GunPosition.position, this.GunPosition.rotation, 0.25f, 0.5f, directionEvent);
				this.m_FSM.SendEvent("Shoot");
			}
		}

        base.Update();
	}
}