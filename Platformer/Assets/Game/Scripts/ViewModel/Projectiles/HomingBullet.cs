using System;
using System.Collections;
using UnityEngine;

/*
A class that inherits from bullet but with different logic for update
 */
public class HomingBullet : Bullet {

    /*
    Target of the bullet
     */
    private GameObject m_Target;

    /*
    Initializes the target
     */
    private void Start() {
        m_Target = GameObject.FindGameObjectWithTag("Player");
    }


    public override void Update() {
        if(m_Target == null)
            return;
        
        var rotation = Quaternion.LookRotation(m_Target.transform.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime);
        transform.Translate(Vector3.forward * ProjectileSpeed * Time.deltaTime * 0.25f);
	}
}