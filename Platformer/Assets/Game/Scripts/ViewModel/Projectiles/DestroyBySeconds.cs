using System;
using System.Collections;
using UnityEngine;

/*
Simple timed destroyer
 */
public class DestroyBySeconds : MonoBehaviour {
    public float TimeToDestroy;
    public void Awake() {
        StartCoroutine(TimedExecute());
    }

    IEnumerator TimedExecute() {
        float currentTime = 0;
        while(currentTime < TimeToDestroy) {
            currentTime += Time.deltaTime;
            yield return null;
        }

        Destroy(this.gameObject);
    }
}