
/*
Interface for all projectile mechanics of the game
 */
public interface IProjectile {
    float ProjectileSpeed { get; set; }
    int Damage { get; set; }
}