using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
Base class of all projectile objects in the game
 */
public class Projectile : MonoBehaviour, IProjectile {

    /*
    Speed of the projectile
     */
    private float m_ProjectileSpeed;
	public float ProjectileSpeed {
        get {
            return m_ProjectileSpeed;
        } set {
            m_ProjectileSpeed = value;
        }
    }

    /*
    Damage of the projectile
     */
    private int m_Damage;
	public int Damage {
        get {
            return m_Damage;
        } set {
            m_Damage = value;
        }
    }
}
