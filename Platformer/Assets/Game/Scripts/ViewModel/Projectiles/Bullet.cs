﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
Simple class that moves object
 */
public class Bullet : Projectile {
	public virtual void Update() {
		this.transform.Translate(Vector3.forward * ProjectileSpeed * Time.deltaTime);
	}
}
