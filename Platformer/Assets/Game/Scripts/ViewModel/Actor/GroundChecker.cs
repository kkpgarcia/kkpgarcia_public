using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundChecker : MonoBehaviour {
    public bool IsGrounded = false;
    /*
    Specifies the objects that are specified as ground
     */
    public LayerMask GroundLayer;

    /*
    Checks if the object is grounded
     */
	public void OnTriggerStay(Collider collision) {
		if(!IsGrounded)
			OnTriggerEnter(collision);
	}

	public void OnTriggerEnter(Collider collision) {
        if(IsOnGroundLayer(collision.gameObject))
            IsGrounded = true;
        else
            IsGrounded = false;
	}

    /*
    Checks if the object is not on the ground
     */
	public void OnTriggerExit(Collider collision) {
        if(IsOnGroundLayer(collision.gameObject))
            IsGrounded = false; 
	}

    private bool IsOnGroundLayer(GameObject obj) {
        int layer = 1 << obj.layer;
        bool onGround = false;

        if(layer == GroundLayer.value)
            onGround = true;
        else
            onGround = false;
        
        return onGround;
    }
}