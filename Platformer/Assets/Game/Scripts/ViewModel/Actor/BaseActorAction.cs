﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
Base class for Actions
 */
public class BaseActorAction : MonoBehaviour, IAction {
	public virtual void OnAction(int damage, int attackSpeed)  { }
}
