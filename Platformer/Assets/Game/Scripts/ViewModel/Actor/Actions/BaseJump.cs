﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BaseJump : MonoBehaviour, IJump {
	
	/*
	Direction, and power of the jump
	 */
	private float m_JumpForce;
	public float JumpForce {
		get {
			return m_JumpForce;
		} set {
			m_JumpForce = value;
		}
	}

	/*
	Rigid Body reference
	 */
	protected Rigidbody m_Rigidbody;
	protected bool m_JumpLock = false;
	protected float m_LockElapse = 0.1f;

	public void Awake() {
		m_Rigidbody = GetComponent<Rigidbody>();
	}

	public virtual void OnJump(bool isGrounded) {
		StartCoroutine(UnlockJump());
	}

	/*
	Unlocks Jump after a certain time.
	 */
	IEnumerator UnlockJump() {
		float currentTime = 0;
		while(currentTime < m_LockElapse) {
			currentTime += Time.deltaTime;
			yield return null;
		}
		m_JumpLock = false;
	}
}
