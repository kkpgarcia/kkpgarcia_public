using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootAction : MonoBehaviour, IAction {
    public Bullet Bullet;
    public Transform GunPosition;
    
	public virtual void OnAction(int damage, int attackSpeed)  {
        Bullet b = Instantiate(Bullet.gameObject, GunPosition.position, GunPosition.rotation).GetComponent<Bullet>();
        b.Damage = damage;
        b.ProjectileSpeed = attackSpeed;
    }
}
