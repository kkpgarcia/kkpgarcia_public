using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SingleJump : BaseJump {

	/*
	Make the Actor Jump
	 */
	public override void OnJump(bool isGrounded) {
		if(m_Rigidbody==null)
			return;
		if(isGrounded && !m_JumpLock) {
			m_JumpLock = true;
			m_Rigidbody.AddForce(new Vector2(0, JumpForce), ForceMode.Impulse);
            base.OnJump(isGrounded);
		}
	}
}
