﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Movement : MonoBehaviour, IMovement {
	private float m_Speed;
	public float Speed {
		get {
			return m_Speed;
		} set {
			m_Speed = value;
		}
	}
	
	public float OnJumpSpeedPercentage;
	Rigidbody m_Rigidbody;

	public void Awake() {
		m_Rigidbody = this.GetComponent<Rigidbody>();	
	}

	public void OnMove(int direction, bool isGrounded, GameObject view) {
		if(m_Rigidbody == null)
			return;
			
		if(direction == 0) {
			if(isGrounded)
				m_Rigidbody.velocity = Vector3.zero;
			else 
				m_Rigidbody.velocity = new Vector3(0, m_Rigidbody.velocity.y, 0);
			
			return;
		}
		
		ChangeDirection(direction, view);
		
		if(isGrounded) {
			m_Rigidbody.velocity = new Vector3(direction * Speed, m_Rigidbody.velocity.y, 0);
		} else {
			m_Rigidbody.velocity = new Vector3(direction * (Speed * OnJumpSpeedPercentage), m_Rigidbody.velocity.y, 0);
		} 
	}

	public void ChangeDirection(int axis, GameObject view) {
		float rotation = axis == -1 ? 180 : 0;
		view.transform.rotation = new Quaternion(0, rotation, 0, 0);
		//this.gameObject.transform.Rotate(new Vector3(0, rotation ,0), Space.Self);
	}
}
