public interface IAction {
    void OnAction(int damage, int attackSpeed);
}