using UnityEngine;

public interface IMovement {
    float Speed { get; set; }
    void OnMove(int direction, bool isGrounded, GameObject view);
    void ChangeDirection(int axis, GameObject view);
}