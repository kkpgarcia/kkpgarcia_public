using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MultiJump : BaseJump {
    private const int MAX_JUMP = 2;
    private int m_currentJumps = 0;

    /*
	Make the Actor Jump
	 */
	public override void OnJump(bool isGrounded) {
		if(isGrounded || !m_JumpLock) {
            m_JumpLock = true;
			m_Rigidbody.AddForce(new Vector2(0, JumpForce), ForceMode.Impulse);
            ++m_currentJumps;

            if(m_currentJumps >= 2) {
                m_currentJumps = 0;
                base.OnJump(isGrounded);
            }
		}
	}
}
