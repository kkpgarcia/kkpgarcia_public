public interface IJump {
    float JumpForce { get; set; }
    void OnJump(bool isGrounded);
    
}