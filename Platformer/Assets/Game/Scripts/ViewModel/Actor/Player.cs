﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
Class composition for actors that are controlled by players.
 */
public class Player : BaseActor {
	/*
	Specified action of the player
	 */
	private IAction m_Action;

	/*
	Specified movement of the player
	 */
	private IMovement m_Movement;

	/*
	Specified Jump of the player
	 */
	private IJump m_Jump;

	/*
	View reference of the object
	 */
	private GameObject m_View;

	/*
	Ground checker for jump mechanics
	 */
	private GroundChecker m_GroundChecker;


	public const string OnInitializeNotfication = "Player.OnInitialize";
	public const string OnHitNotfication = "Player.OnHit";
	
	public void Awake() {
		m_View = this.transform.Find("View").gameObject;
		m_GroundChecker = this.transform.Find("Ground Checker").GetComponent<GroundChecker>();
	}

	public void MoveHorizontal(int x) {
		if(m_Movement != null)
			m_Movement.OnMove(x, m_GroundChecker.IsGrounded, m_View);
	}

	public void MoveVertical(int y) { }

	public void OnJump(int y) {
		if(m_Jump != null)
			m_Jump.OnJump(m_GroundChecker.IsGrounded);
	}

	public void OnAction(int x) {
		m_Action.OnAction(Stats[(int)StatTypes.POWER], Stats[(int)StatTypes.AttackSpeed]);
	}
	
	/*
	Applies status from the .asset file from resources. Should make use of an importer class instead of
	passing it here.
	 */
	public override void ApplyStats(ActorData data) {
		m_Jump = GetComponent<IJump>();
		m_Movement = GetComponent<IMovement>();
		m_Action = GetComponent<IAction>();

		m_Movement.Speed = data.Speed;
		m_Jump.JumpForce = data.JumpForce;
		this.PostNotification(OnInitializeNotfication, data);
		base.ApplyStats(data);
	}

	/*
	Simple and quick collision detection
	 */
	private void OnTriggerEnter(Collider collider) {
		GameObject go = collider.gameObject;
		if(go.tag == "Enemy Bullet"){
			Bullet bullet = go.GetComponent<Bullet>();
			OnHit(bullet.Damage);
			Destroy(go);
		}
	}

	void OnHit(int damage) {
		this.Stats[(int)StatTypes.HP] -= damage;
		this.PostNotification(OnHitNotfication, damage);

		if(this.Stats[(int)StatTypes.HP] <= 0) {
			DestroyActor();
		}
	}

	void DestroyActor() {
		this.PostNotification(OnHitNotfication, 100);
		UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
		Destroy(this.gameObject);
	}

}
