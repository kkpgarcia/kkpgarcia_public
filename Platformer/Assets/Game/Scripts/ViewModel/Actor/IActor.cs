public interface IActor {
    void ApplyStats(ActorData data);
}