using UnityEngine;

/*
Base actor for all enemies, and players
 */
public class BaseActor : MonoBehaviour, IActor {

    public static readonly StatTypes[] StatOrder = new StatTypes[] {
        StatTypes.MHP,
        StatTypes.HP,
        StatTypes.POWER,
        StatTypes.AttackSpeed
    };

    [HideInInspector]
    public int[] Stats;

    public virtual void ApplyStats(ActorData data) {
        Stats = new int[StatOrder.Length];
        Stats[(int)StatTypes.MHP] = data.MHP;
        Stats[(int)StatTypes.HP] = data.HP;
        Stats[(int)StatTypes.POWER] = data.Power;
        Stats[(int)StatTypes.AttackSpeed] = data.AttackSpeed;
    }
}