using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
public struct Point : IEquatable<Point> {
    #region Fields
	public int X;
	public int Y;
	#endregion

	#region Constructors
	public Point (int x, int y)
	{
		this.X = x;
		this.Y = y;
	}
	#endregion

	#region Operator Overloads
	public static Point operator +(Point a, Point b)
	{
		return new Point(a.X + b.X, a.Y + b.Y);
	}
	
	public static Point operator -(Point p1, Point p2) 
	{
		return new Point(p1.X - p2.X, p1.Y - p2.Y);
	}

	public static Point operator *(Point p1, Point p2) {
		return new Point(p1.X * p2.X, p1.Y * p2.Y);
	}
	
	public static bool operator ==(Point a, Point b)
	{
		return a.X == b.X && a.Y == b.Y;
	}
	
	public static bool operator !=(Point a, Point b)
	{
		return !(a == b);
	}

	public static implicit operator Vector2(Point p)
	{
		return new Vector2(p.X, p.Y);
	}
	#endregion

	#region Object Overloads
	public override bool Equals (object obj)
	{
		if (obj is Point)
		{
			Point p = (Point)obj;
			return X == p.X && Y == p.Y;
		}
		return false;
	}
	
	public bool Equals (Point p)
	{
		return X == p.X && Y == p.Y;
	}
	
	public override int GetHashCode ()
	{
		return X ^ Y;
	}

	public override string ToString ()
	{
		return string.Format ("({0},{1})", X, Y);
	}
	#endregion
}