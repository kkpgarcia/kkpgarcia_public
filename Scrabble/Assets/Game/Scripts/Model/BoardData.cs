using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoardData : ScriptableObject {
    public List<Vector3> Tiles;
}