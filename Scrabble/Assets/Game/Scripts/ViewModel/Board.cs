using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Board : MonoBehaviour {
    public Dictionary<Point, Tile> Tiles = new Dictionary<Point, Tile>();
    public GameObject TilePrefab;
    public int TileSize;

    public Rect Container;

    public void Create() {
        for(int i = -(TileSize/2); i < (TileSize/2); ++i) {
            for(int j = -(TileSize/2); j < (TileSize/2); ++j) {
                GameObject instance = Instantiate(TilePrefab, new Vector3(i, j, 0), this.transform.rotation) as GameObject;
                instance.transform.SetParent(transform);
                Tile t = instance.GetComponent<Tile>();
                t.Position = new Point(i, j);
                Tiles.Add(t.Position, t);
            }
        }

        Container = new Rect((this.transform as RectTransform).rect);
    }
}