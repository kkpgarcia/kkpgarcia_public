﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class LetterRack : MonoBehaviour {
	public Dictionary<Point, LetterTile> Tiles = new Dictionary<Point, LetterTile>();
	public GameObject TilePrefab;
	public RectTransform ParentTransform;

	public Rect Container;

	public void Create() {
		StartCoroutine(OnCreate());
	}

	IEnumerator OnCreate() {
		for(int i = -4; i < 3; ++i) {
			GameObject instance = Instantiate(TilePrefab);
			instance.transform.SetParent(ParentTransform);
			LetterTile t = instance.GetComponent<LetterTile>();
			yield return null;
			AddTile(t);
			
		}
		Container = new Rect((this.transform as RectTransform).rect);
	}

	//Still buggy when returning tiles. Should watch out for the keys of this dictionary.
	public void AddTile(LetterTile t) {
		if(Tiles.ContainsKey(t.Position))
			Tiles.Remove(t.Position);
		
		t.Position = new Point(6, Mathf.CeilToInt(t.transform.position.y));
		Tiles.Add(t.Position, t);
	}

	private void RecalculateTiles() {
		Dictionary<Point, LetterTile> newDict = new Dictionary<Point, LetterTile>();
		foreach(KeyValuePair<Point, LetterTile> kv in Tiles) {
			Vector2 v = kv.Value.transform.position;

			if(kv.Key.X != Mathf.CeilToInt(v.x) && kv.Key.Y != Mathf.CeilToInt(v.y)) {
				newDict.Add(new Point(6, Mathf.CeilToInt(v.y)), kv.Value);
			}
			newDict.Add(kv.Key, kv.Value);
		}
		Tiles = newDict;
	}

	public LetterTile GetTile(Point p) {
		Debug.Log(p);
		try {
			LetterTile curr = Tiles[p];
			Tiles.Remove(p);
			RecalculateTiles();
			return curr;
		} catch (KeyNotFoundException e) {
			return null;
		}
	}
}
