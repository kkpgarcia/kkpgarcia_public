using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LetterTile : Tile {
    public string Letter;
    public string Value;

    public void SnapToBoard(Tile tile) {
        this.transform.SetParent(tile.transform.parent);
        this.transform.position = tile.Center;
    }

    public void SnapToRack(LetterRack rack) {
        this.transform.SetParent(rack.transform);
        rack.AddTile(this);
    }

    public void Drag(Vector2 pos) {
        this.transform.position = pos;
    }

    public void Detach() {
        this.transform.SetParent(this.transform.parent.parent);
    }
}