using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {
    public Point Position;
    public Vector3 Center {
        get {
            return new Vector3(Position.X, Position.Y, 0);
        }
    }

}