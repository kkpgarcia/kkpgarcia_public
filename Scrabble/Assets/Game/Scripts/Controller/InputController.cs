using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class InputController : MonoBehaviour {
    public static event EventHandler<InfoEventArgs<Vector2>> OnDragEvent;
    public static event EventHandler<InfoEventArgs<Vector2>> OnDropEvent;
    //Removed Pan Event On My Input Handler because it was unnecessary.

    void Update() {
        //Start Drag
        if(Input.GetMouseButton(0)) {
            if(OnDragEvent != null) 
                OnDragEvent(this, new InfoEventArgs<Vector2>(Input.mousePosition));
            //Drag Release
        } else if (Input.GetMouseButtonUp(0)) {
            if(OnDropEvent != null)
                OnDropEvent(this, new InfoEventArgs<Vector2>(Input.mousePosition));
        }
    }
}
