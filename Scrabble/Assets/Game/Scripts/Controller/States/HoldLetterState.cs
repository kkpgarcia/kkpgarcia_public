using System;
using System.Collections;
using UnityEngine;

public class HoldLetterState : GameState {
    public override void Enter() {
        base.Enter();
    }    
    protected override void OnDrag(object sender, InfoEventArgs<Vector2> info) {
        Vector2 pos = Camera.main.ScreenToWorldPoint(info.Info);
        Owner.HoldingTile.Drag(pos);
    }

    protected override void OnDrop(object sender, InfoEventArgs<Vector2> info) {
        Vector2 pos = Camera.main.ScreenToWorldPoint(info.Info);
        if(Owner.Board.Container.Contains(pos)) {
            //From Board
            Debug.Log("From Board");
            Point p = new Point(Mathf.CeilToInt(pos.x), Mathf.CeilToInt(pos.y));
            if(Board.Tiles.ContainsKey(p)) {
                Owner.HoldingTile.SnapToBoard(Board.Tiles[p]);
            } else {
                Owner.HoldingTile.SnapToRack(Owner.LetterRack);
            }
        } else {
            Debug.Log("Not Board");
            Owner.HoldingTile.SnapToRack(Owner.LetterRack);
        }
        Owner.HoldingTile = null;
        Owner.ChangeState<LetterSelectionState>();
    }
}