using System;
using System.Collections;
using UnityEngine;

public class InitGameState : GameState {
    public override void Enter() {
		base.Enter();
        StartCoroutine(Load());
        Debug.Log("Init Enter");
	}

    IEnumerator Load() {
        this.Board.Create();
        this.LetterRack.Create();
        yield return null;
        Owner.ChangeState<LetterSelectionState>();
    }
}