using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : State {
    protected GameController Owner;
    protected Board Board {
        get {
            return Owner.Board;
        }
    }

    protected LetterRack LetterRack {
        get {
            return Owner.LetterRack;
        }
    }

    protected LetterTile HoldingTile {
        get {
            return Owner.HoldingTile;
        }
    }

    protected virtual void Awake ()
	{
		Owner = GetComponent<GameController>();
	}

	protected override void AddListeners() {
        InputController.OnDragEvent += OnDrag;
        InputController.OnDropEvent += OnDrop;
    }

	protected override void RemoveListeners() {
        InputController.OnDragEvent -= OnDrag;
        InputController.OnDropEvent -= OnDrop;
	}

    protected virtual void OnDrag(object sender, InfoEventArgs<Vector2> info) { }
    protected virtual void OnDrop(object sender, InfoEventArgs<Vector2> info) { }
}
