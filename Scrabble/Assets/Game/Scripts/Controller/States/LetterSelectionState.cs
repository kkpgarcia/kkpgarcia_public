using System;
using System.Collections;
using UnityEngine;

public class LetterSelectionState : GameState {
    public override void Enter() {
        base.Enter();
    }    
    protected override void OnDrag(object sender, InfoEventArgs<Vector2> info) {
        Vector2 pos = Camera.main.ScreenToWorldPoint(info.Info);
        LetterTile t = null;
        
        if(Owner.Board.Container.Contains(pos)) {
            //From Board
            Debug.Log("From Board");
            
        } else {
            Debug.Log("Not Board");
            t = LetterRack.GetTile(new Point(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y)));
        }

        if(t == null)
            return;
            
        Owner.HoldingTile = t;
        t.Detach();
        Owner.ChangeState<HoldLetterState>();
    }
}