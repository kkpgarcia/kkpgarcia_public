using System;
using System.Collections;
using UnityEngine;

public class GameController : StateMachine {
    public Board Board;
    public LetterRack LetterRack;
    public LetterTile HoldingTile;
    private void Start() {
        ChangeState<InitGameState>();
    }
}