﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour {
		
	protected State m_currentState;
	public virtual State CurrentState {
		get {
			return m_currentState;
		} set {
			Transition (value);
		}
	}

	public bool m_inTransition;

	public virtual T GetState<T>() where T : State {
		T target = GetComponent<T> ();
		if (target == null)
			target = gameObject.AddComponent<T> ();
		return target;
	}

	public virtual void ChangeState<T>() where T : State {
		CurrentState = GetState<T> ();
	}

	protected virtual void Transition (State value) {
		if (m_currentState == value || m_inTransition)
			return;

		m_inTransition = true;

		if (m_currentState != null)
			m_currentState.Exit ();

		m_currentState = value;

		if (m_currentState != null)
			m_currentState.Enter ();

		m_inTransition = false;
	}
}
